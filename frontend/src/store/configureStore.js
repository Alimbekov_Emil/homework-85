import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import { loadFromLocalStorage, saveToLocalStorage } from "./localStorage";
import trackHistoryReducer from "./reducers/trackHistoryReducer";
import albumsReducer from "./reducers/albumsReducer";
import artistReducer from "./reducers/artistReducer";
import trackReducer from "./reducers/trackReducer";
import usersReducer, { initialState } from "./reducers/usersReducer";
import axiosMusic from "../axiosMusic";
import adminsReducer from "./reducers/adminsReducer";

const rootReducer = combineReducers({
  users: usersReducer,
  trackHistory: trackHistoryReducer,
  albums: albumsReducer,
  artist: artistReducer,
  tracks: trackReducer,
  admins: adminsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
  saveToLocalStorage({
    users: {
      ...initialState,
      user: store.getState().users.user,
    },
  });
});

axiosMusic.interceptors.request.use((config) => {
  try {
    config.headers["Authorization"] = store.getState().users.user.token;
  } catch (e) {
    // do nothing, no token exists
  }

  return config;
});

axiosMusic.interceptors.response.use(
  (res) => res,
  (e) => {
    if (!e.response) {
      e.response = { data: { global: "No internet" } };
    }

    throw e;
  }
);

export default store;

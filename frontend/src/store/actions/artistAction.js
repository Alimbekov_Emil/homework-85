import axiosMusic from "../../axiosMusic";
import { NotificationManager } from "react-notifications";
import { historyPush } from "./historyActions";

export const FETCH_ARTISTS_REQUEST = "FETCH_ARTISTS_REQUEST";
export const FETCH_ARTISTS_SUCCESS = "FETCH_ARTISTS_SUCCESS";
export const FETCH_ARTISTS_FAILURE = "FETCH_ARTISTS_FAILURE";

export const CREATE_ARTIST_REQUEST = "CREATE_ARTIST_REQUEST";
export const CREATE_ARTIST_SUCCESS = "CREATE_ARTIST_SUCCESS";
export const CREATE_ARTIST_FAILURE = "CREATE_ARTIST_FAILURE";

export const fetchArtistsRequest = () => ({ type: FETCH_ARTISTS_REQUEST });
export const fetchArtistsSuccess = (artists) => ({ type: FETCH_ARTISTS_SUCCESS, artists });
export const fetchArtistsFailure = (error) => ({ type: FETCH_ARTISTS_FAILURE, error });

export const createArtistRequest = () => ({ type: CREATE_ARTIST_REQUEST });
export const createArtistSuccess = () => ({ type: CREATE_ARTIST_SUCCESS });
export const createArtistFailure = (error) => ({ type: CREATE_ARTIST_FAILURE, error });

export const fetchArtists = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchArtistsRequest());
      const response = await axiosMusic.get("/artists");
      dispatch(fetchArtistsSuccess(response.data));
    } catch (e) {
      dispatch(fetchArtistsFailure(e));
    }
  };
};

export const createArtist = (artistData) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      dispatch(createArtistRequest());
      await axiosMusic.post("/artists", artistData, { headers: { Authorization: token } });
      dispatch(createArtistSuccess());
      NotificationManager.success("Add New Artist");
      dispatch(historyPush("/"));
    } catch (e) {
      dispatch(createArtistFailure(e));
      NotificationManager.error("Error");
    }
  };
};

import axiosMusic from "../../axiosMusic";
import { historyPush } from "./historyActions";

export const POST_TRACK_HISTORY_REQUEST = "POST_TRACK_HISTORY_REQUEST";
export const POST_TRACK_HISTORY_SUCCESS = "POST_TRACK_HISTORY_SUCCESS";
export const POST_TRACK_HISTORY_FAILURE = "POST_TRACK_HISTORY_FAILURE";

export const FETCH_TRACK_HISTORY_REQUEST = "FETCH_TRACK_HISTORY_REQUEST";
export const FETCH_TRACK_HISTORY_SUCCESS = "FETCH_TRACK_HISTORY_SUCCESS";
export const FETCH_TRACK_HISTORY_FAILURE = "FETCH_TRACK_HISTORY_FAILURE";

export const fetchTrackHistoryRequest = () => ({ type: FETCH_TRACK_HISTORY_REQUEST });
export const fetchTrackHistorySuccess = (trackHistory) => ({
  type: FETCH_TRACK_HISTORY_SUCCESS,
  trackHistory,
});
export const fetchTrackHistoryFailure = (error) => ({ type: FETCH_TRACK_HISTORY_FAILURE, error });

export const postTrackHistoryRequest = () => ({ type: POST_TRACK_HISTORY_REQUEST });
export const postTrackHistorySuccess = () => ({ type: POST_TRACK_HISTORY_SUCCESS });
export const postTrackHistoryFailure = (error) => ({ type: POST_TRACK_HISTORY_FAILURE, error });

export const postTrackHistory = (id) => {
  return async (dispatch, getState) => {
    const user = getState().users.user;
    if (user === null) {
      dispatch(historyPush("/login"));
    } else {
      try {
        await axiosMusic.post("/track_history", { track: id }, { headers: { Authorization: user.token } });
        dispatch(postTrackHistorySuccess());
      } catch (e) {}
    }
  };
};

export const fetchTrackHistory = () => {
  return async (dispatch, getState) => {
    const user = getState().users.user;
    if (user === null) {
      dispatch(historyPush("/login"));
    } else {
      try {
        dispatch(fetchTrackHistoryRequest());
        const response = await axiosMusic.get("/track_history", { headers: { Authorization: user.token } });
        dispatch(fetchTrackHistorySuccess(response.data));
      } catch (e) {
        dispatch(fetchTrackHistoryFailure(e));
      }
    }
  };
};

import axiosMusic from "../../axiosMusic";
import { historyPush } from "./historyActions";
import { NotificationManager } from "react-notifications";

export const FETCH_TRACKS_REQUEST = "FETCH_TRACKS_REQUEST";
export const FETCH_TRACKS_SUCCESS = "FETCH_TRACKS_SUCCESS";
export const FETCH_TRACKS_FAILURE = "FETCH_TRACKS_FAILURE";

export const CREATE_TRACKS_REQUEST = "CREATE_TRACKS_REQUEST";
export const CREATE_TRACKS_SUCCESS = "CREATE_TRACKS_SUCCESS";
export const CREATE_TRACKS_FAILURE = "CREATE_TRACKS_FAILURE";

export const fetchTracksRequest = () => ({ type: FETCH_TRACKS_REQUEST });
export const fetchTracksSuccess = (tracks) => ({ type: FETCH_TRACKS_SUCCESS, tracks });
export const fetchTracksFailure = (error) => ({ type: FETCH_TRACKS_FAILURE, error });

export const createTracksRequest = () => ({ type: CREATE_TRACKS_REQUEST });
export const createTracksSuccess = () => ({ type: CREATE_TRACKS_SUCCESS });
export const createTracksFailure = (error) => ({ type: CREATE_TRACKS_FAILURE, error });

export const fetchTracks = (id) => {
  return async (dispatch) => {
    try {
      dispatch(fetchTracksRequest());
      const response = await axiosMusic.get("/tracks?album=" + id);
      dispatch(fetchTracksSuccess(response.data));
    } catch (e) {
      dispatch(fetchTracksFailure(e));
    }
  };
};

export const createTracks = (trackData) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      dispatch(createTracksRequest());
      await axiosMusic.post("/tracks", trackData, { headers: { Authorization: token } });
      dispatch(createTracksSuccess());
      NotificationManager.success("Add New Track");

      dispatch(historyPush("/"));
    } catch (e) {
      dispatch(createTracksFailure(e));
      NotificationManager.error("Error");
    }
  };
};

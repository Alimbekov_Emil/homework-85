import axiosMusic from "../../axiosMusic";
import { NotificationManager } from "react-notifications";
import { fetchArtists } from "./artistAction";

export const FETCH_UNPUBLISHED_ALBUMS_REQUEST = "FETCH_UNPUBLISHED_ALBUMS_REQUEST";
export const FETCH_UNPUBLISHED_ALBUMS_SUCCESS = "FETCH_UNPUBLISHED_ALBUMS_SUCCESS";
export const FETCH_UNPUBLISHED_ALBUMS_FAILURE = "FETCH_UNPUBLISHED_ALBUMS_FAILURE";

export const FETCH_UNPUBLISHED_ARTISTS_REQUEST = "FETCH_UNPUBLISHED_ARTISTS_REQUEST";
export const FETCH_UNPUBLISHED_ARTISTS_SUCCESS = "FETCH_UNPUBLISHED_ARTISTS_SUCCESS";
export const FETCH_UNPUBLISHED_ARTISTS_FAILURE = "FETCH_UNPUBLISHED_ARTISTS_FAILURE";

export const FETCH_UNPUBLISHED_TRACKS_REQUEST = "FETCH_UNPUBLISHED_TRACKS_REQUEST";
export const FETCH_UNPUBLISHED_TRACKS_SUCCESS = "FETCH_UNPUBLISHED_TRACKS_SUCCESS";
export const FETCH_UNPUBLISHED_TRACKS_FAILURE = "FETCH_UNPUBLISHED_TRACKS_FAILURE";

export const DELETE_UNPUBLISHED_ALBUMS_SUCCESS = "DELETE_UNPUBLISHED_ALBUMS_SUCCESS";
export const DELETE_UNPUBLISHED_ARTISTS_SUCCESS = "DELETE_UNPUBLISHED_ARTISTS_SUCCESS";
export const DELETE_UNPUBLISHED_TRACKS_SUCCESS = "DELETE_UNPUBLISHED_TRACKS_SUCCESS";

export const TOPUBLISH_ALBUMS_SUCCESS = "TOPUBLISH_ALBUMS_SUCCESS";
export const TOPUBLISH_ARTISTS_SUCCESS = "TOPUBLISH_ARTISTS_SUCCESS";
export const TOPUBLISH_TRACKS_SUCCESS = "TOPUBLISH_TRACKS_SUCCESS";

export const fetchUnPublishedAlbumsRequest = () => ({ type: FETCH_UNPUBLISHED_ALBUMS_REQUEST });
export const fetchUnPublishedAlbumsSuccess = (albums) => ({ type: FETCH_UNPUBLISHED_ALBUMS_SUCCESS, albums });
export const fetchUnPublishedAlbumsFailure = (error) => ({ type: FETCH_UNPUBLISHED_ALBUMS_FAILURE, error });

export const fetchUnPublishedArtistsRequest = () => ({ type: FETCH_UNPUBLISHED_ARTISTS_REQUEST });
export const fetchUnPublishedArtistsSuccess = (artists) => ({
  type: FETCH_UNPUBLISHED_ARTISTS_SUCCESS,
  artists,
});
export const fetchUnPublishedArtistsFailure = (error) => ({ type: FETCH_UNPUBLISHED_ARTISTS_FAILURE, error });

export const fetchUnPublishedTracksRequest = () => ({ type: FETCH_UNPUBLISHED_TRACKS_REQUEST });
export const fetchUnPublishedTracksSuccess = (tracks) => ({ type: FETCH_UNPUBLISHED_TRACKS_SUCCESS, tracks });
export const fetchUnPublishedTracksFailure = (error) => ({ type: FETCH_UNPUBLISHED_TRACKS_FAILURE, error });

export const deleteUnPublishedAlbumsSuccess = () => ({ type: DELETE_UNPUBLISHED_ALBUMS_SUCCESS });
export const deleteUnPublishedArtistsSuccess = () => ({
  type: DELETE_UNPUBLISHED_ARTISTS_SUCCESS,
});
export const deleteUnPublishedTracksSuccess = () => ({
  type: DELETE_UNPUBLISHED_TRACKS_SUCCESS,
});

export const fetchUnPublushidAlbums = () => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      dispatch(fetchUnPublishedAlbumsRequest());
      const response = await axiosMusic.get("/albums/admin", { headers: { Authorization: token } });
      dispatch(fetchUnPublishedAlbumsSuccess(response.data));
    } catch (e) {
      dispatch(fetchUnPublishedAlbumsFailure(e));
    }
  };
};

export const fetchUnPublushidArtists = () => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      dispatch(fetchUnPublishedArtistsRequest());
      const response = await axiosMusic.get("/artists/admin", { headers: { Authorization: token } });
      dispatch(fetchUnPublishedArtistsSuccess(response.data));
    } catch (e) {
      dispatch(fetchUnPublishedArtistsFailure(e));
    }
  };
};

export const fetchUnPublushidTracks = () => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      dispatch(fetchUnPublishedTracksRequest());
      const response = await axiosMusic.get("/tracks/admin", { headers: { Authorization: token } });
      dispatch(fetchUnPublishedTracksSuccess(response.data));
    } catch (e) {
      dispatch(fetchUnPublishedTracksFailure(e));
    }
  };
};

export const deleteTracks = (id) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      await axiosMusic.delete("/tracks/admin/" + id, { headers: { Authorization: token } });
      dispatch(deleteUnPublishedTracksSuccess());
      dispatch(fetchUnPublushidTracks());
    } catch (e) {}
  };
};

export const deleteArtist = (id) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      await axiosMusic.delete("/artists/admin/" + id, { headers: { Authorization: token } });
      dispatch(deleteUnPublishedArtistsSuccess());
      dispatch(fetchUnPublushidArtists());
      dispatch(fetchArtists());
    } catch (e) {}
  };
};

export const deleteAlbum = (id) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      await axiosMusic.delete("/albums/admin/" + id, { headers: { Authorization: token } });
      dispatch(deleteUnPublishedAlbumsSuccess());
      dispatch(fetchUnPublushidAlbums());
    } catch (e) {}
  };
};

export const toPublishArtist = (id) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      await axiosMusic.post("/artists/admin/publish/" + id, {}, { headers: { Authorization: token } });
      dispatch(fetchUnPublushidArtists());
    } catch (e) {}
  };
};

export const toPublishAlbum = (id) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      await axiosMusic.post("/albums/admin/publish/" + id, {}, { headers: { Authorization: token } });
      dispatch(fetchUnPublushidAlbums());
    } catch (e) {}
  };
};

export const toPublishTrack = (id) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      await axiosMusic.post("/tracks/admin/publish/" + id, {}, { headers: { Authorization: token } });
      dispatch(fetchUnPublushidTracks());
    } catch (e) {}
  };
};

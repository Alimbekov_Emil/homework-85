import axiosMusic from "../../axiosMusic";
import { NotificationManager } from "react-notifications";
import { historyPush } from "./historyActions";

export const FETCH_ALBUMS_REQUEST = "FETCH_ALBUMS_REQUEST";
export const FETCH_ALBUMS_SUCCESS = "FETCH_ALBUMS_SUCCESS";
export const FETCH_ALBUMS_FAILURE = "FETCH_ALBUMS_FAILURE";

export const CREATE_ALBUM_REQUEST = "CREATE_ALBUM_REQUEST";
export const CREATE_ALBUM_SUCCESS = "CREATE_ALBUM_SUCCESS";
export const CREATE_ALBUM_FAILURE = "CREATE_ALBUM_FAILURE";

export const fetchAlbumsRequest = () => ({ type: FETCH_ALBUMS_REQUEST });
export const fetchAlbumsSuccess = (albums) => ({ type: FETCH_ALBUMS_SUCCESS, albums });
export const fetchAlbumsFailure = (error) => ({ type: FETCH_ALBUMS_FAILURE, error });

export const createAlbumsRequest = () => ({ type: CREATE_ALBUM_REQUEST });
export const createAlbumsSuccess = () => ({ type: CREATE_ALBUM_SUCCESS });
export const createAlbumsFailure = (error) => ({ type: CREATE_ALBUM_FAILURE, error });

export const fetchAlbums = (albumId) => {
  return async (dispatch) => {
    let url = "/albums";

    if (albumId) {
      url += "?artist=" + albumId;
    }

    try {
      dispatch(fetchAlbumsRequest());
      const response = await axiosMusic.get(url);
      dispatch(fetchAlbumsSuccess(response.data));
    } catch (e) {
      dispatch(fetchAlbumsFailure(e));
    }
  };
};

export const createAlbum = (albumData) => {
  return async (dispatch, getState) => {
    const token = getState().users.user.token;
    if (token === null) {
      NotificationManager.warning("You are not logged in");
    }
    try {
      dispatch(createAlbumsRequest());
      await axiosMusic.post("/albums", albumData, { headers: { Authorization: token } });
      dispatch(createAlbumsSuccess());
      NotificationManager.success("Add New Albums");
      dispatch(historyPush("/"));
    } catch (e) {
      dispatch(createAlbumsFailure(e));
      NotificationManager.error("Error");
    }
  };
};

import {
  DELETE_UNPUBLISHED_ALBUMS_SUCCESS,
  DELETE_UNPUBLISHED_ARTISTS_SUCCESS,
  DELETE_UNPUBLISHED_TRACKS_SUCCESS,
  FETCH_UNPUBLISHED_ALBUMS_SUCCESS,
  FETCH_UNPUBLISHED_ARTISTS_SUCCESS,
  FETCH_UNPUBLISHED_TRACKS_SUCCESS,
  TOPUBLISH_ALBUMS_SUCCESS,
  TOPUBLISH_ARTISTS_SUCCESS,
  TOPUBLISH_TRACKS_SUCCESS,
} from "../actions/adminActions";

const initialState = {
  errorAdmins: false,
  loadingAdmins: false,
  albums: [],
  artists: [],
  tracks: [],
};

const adminsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_UNPUBLISHED_ALBUMS_SUCCESS:
      return { ...state, loadingAdmins: false, albums: action.albums };
    case FETCH_UNPUBLISHED_ARTISTS_SUCCESS:
      return { ...state, loadingAdmins: false, artists: action.artists };
    case FETCH_UNPUBLISHED_TRACKS_SUCCESS:
      return { ...state, loadingAdmins: false, tracks: action.tracks };
    case DELETE_UNPUBLISHED_ARTISTS_SUCCESS:
      return { ...state, loadingAdmins: false };
    case DELETE_UNPUBLISHED_ALBUMS_SUCCESS:
      return { ...state, loadingAdmins: false };
    case DELETE_UNPUBLISHED_TRACKS_SUCCESS:
      return { ...state, loadingAdmins: false };
    case TOPUBLISH_ALBUMS_SUCCESS:
      return { ...state, loadingAdmins: false };
    case TOPUBLISH_TRACKS_SUCCESS:
      return { ...state, loadingAdmins: false };
    case TOPUBLISH_ARTISTS_SUCCESS:
      return { ...state, loadingAdmins: false };
    default:
      return state;
  }
};

export default adminsReducer;

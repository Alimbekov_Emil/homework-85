import {
  FETCH_ARTISTS_REQUEST,
  FETCH_ARTISTS_SUCCESS,
  FETCH_ARTISTS_FAILURE,
  CREATE_ARTIST_REQUEST,
  CREATE_ARTIST_SUCCESS,
  CREATE_ARTIST_FAILURE,
} from "../actions/artistAction";

const initialState = {
  errorArtist: false,
  loadingArtist: false,
  artists: [],
};

const artistReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTISTS_REQUEST:
      return { ...state, loadingArtist: true };
    case FETCH_ARTISTS_SUCCESS:
      return { ...state, loadingArtist: false, artists: action.artists };
    case FETCH_ARTISTS_FAILURE:
      return { ...state, loadingArtist: false, errorArtist: action.error };
    case CREATE_ARTIST_REQUEST:
      return { ...state, loadingArtist: true };
    case CREATE_ARTIST_SUCCESS:
      return { ...state, loadingArtist: false };
    case CREATE_ARTIST_FAILURE:
      return { ...state, loadingArtist: false, errorArtist: action.error };

    default:
      return state;
  }
};

export default artistReducer;

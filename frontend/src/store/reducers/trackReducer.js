import { FETCH_TRACKS_FAILURE, FETCH_TRACKS_REQUEST, FETCH_TRACKS_SUCCESS } from "../actions/tracksActions";

const initialState = {
  errorTracks: false,
  loadingTracks: false,
  tracks: [],
};

const trackReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRACKS_REQUEST:
      return { ...state, loadingTracks: true };
    case FETCH_TRACKS_SUCCESS:
      return { ...state, loadingTracks: false, tracks: action.tracks };
    case FETCH_TRACKS_FAILURE:
      return { ...state, loadingTracks: false, errorTracks: action.error };
    default:
      return state;
  }
};

export default trackReducer;

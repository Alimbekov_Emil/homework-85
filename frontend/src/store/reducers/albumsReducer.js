import {
  CREATE_ALBUM_FAILURE,
  CREATE_ALBUM_REQUEST,
  CREATE_ALBUM_SUCCESS,
  FETCH_ALBUMS_FAILURE,
  FETCH_ALBUMS_REQUEST,
  FETCH_ALBUMS_SUCCESS,
} from "../actions/albumsActions";

const initialState = {
  errorAlbums: false,
  loadingAlbums: false,
  albums: [],
};

const albumsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALBUMS_REQUEST:
      return { ...state, loadingAlbums: true };
    case FETCH_ALBUMS_SUCCESS:
      return { ...state, loadingAlbums: false, albums: action.albums };
    case FETCH_ALBUMS_FAILURE:
      return { ...state, loadingAlbums: false, errorAlbums: action.error };
    case CREATE_ALBUM_REQUEST:
      return { ...state, loadingAlbums: true };
    case CREATE_ALBUM_SUCCESS:
      return { ...state, loadingAlbums: false };
    case CREATE_ALBUM_FAILURE:
      return { ...state, loadingAlbums: false, errorAlbums: action.error };

    default:
      return state;
  }
};

export default albumsReducer;

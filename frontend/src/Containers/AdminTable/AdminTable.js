import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import UnPublished from "./UnPublished";

import {
  fetchUnPublushidAlbums,
  fetchUnPublushidArtists,
  fetchUnPublushidTracks,
} from "../../store/actions/adminActions";
import { Grid } from "@material-ui/core";

const AdminTable = () => {
  const dispatch = useDispatch();

  const albums = useSelector((state) => state.admins.albums);
  const tracks = useSelector((state) => state.admins.tracks);
  const artists = useSelector((state) => state.admins.artists);

  useEffect(() => {
    dispatch(fetchUnPublushidAlbums());
    dispatch(fetchUnPublushidTracks());
    dispatch(fetchUnPublushidArtists());
  }, [dispatch]);

  return (
    <Grid container justify="space-between" spacing={4} style={{ minHeight: "1000px" }}>
      <UnPublished entities={artists} title="Artists" />
      <UnPublished entities={albums} title="Albums" />
      <UnPublished entities={tracks} title="Tracks" />
    </Grid>
  );
};

export default AdminTable;

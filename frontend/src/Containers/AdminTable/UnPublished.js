import { Button, Card, CardActions, CardHeader, Grid, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch } from "react-redux";
import {
  deleteTracks,
  deleteArtist,
  deleteAlbum,
  toPublishArtist,
  toPublishTrack,
  toPublishAlbum,
} from "../../store/actions/adminActions";

const UnPublished = (props) => {
  const dispatch = useDispatch();

  return (
    <Grid item xs style={{ borderRight: "2px solid black" }}>
      <Typography variant="h6">{props.title}</Typography>
      {props.entities.map((essence) => (
        <Card key={essence._id} style={{ margin: "10px 0", border: "1px solid black" }}>
          <CardHeader title={essence.title} />
          <p style={{ margin: "20px" }}> (not published)</p>
          <CardActions>
            <Grid item>
              {props.title === "Tracks" && (
                <>
                  <Button onClick={() => dispatch(toPublishTrack(essence._id))} variant="outlined">
                    to publish
                  </Button>
                  <Button onClick={() => dispatch(deleteTracks(essence._id))} variant="outlined">
                    Delete
                  </Button>
                </>
              )}
              {props.title === "Albums" && (
                <>
                  <Button onClick={() => dispatch(toPublishAlbum(essence._id))} variant="outlined">
                    to publish
                  </Button>
                  <Button onClick={() => dispatch(deleteAlbum(essence._id))} variant="outlined">
                    Delete
                  </Button>
                </>
              )}
              {props.title === "Artists" && (
                <>
                  <Button onClick={() => dispatch(toPublishArtist(essence._id))} variant="outlined">
                    to publish
                  </Button>
                  <Button onClick={() => dispatch(deleteArtist(essence._id))} variant="outlined">
                    Delete
                  </Button>
                </>
              )}
            </Grid>
          </CardActions>
        </Card>
      ))}
    </Grid>
  );
};

export default UnPublished;

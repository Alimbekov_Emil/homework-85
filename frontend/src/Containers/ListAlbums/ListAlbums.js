import { Grid, Typography } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchAlbums } from "../../store/actions/albumsActions";
import Albums from "./Album";

const ListAlbums = (props) => {
  const dispatch = useDispatch();
  const albums = useSelector((state) => state.albums.albums);

  useEffect(() => {
    dispatch(fetchAlbums(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container justify="space-between" alignItems="center">
        <Grid item>
          {albums.length > 0 ? (
            <Typography variant="h4">{albums[0].artist.title}</Typography>
          ) : (
            <Typography>Список Альбомов Пуст</Typography>
          )}
        </Grid>
      </Grid>
      <Grid item container spacing={3}>
        {albums.map((album) => (
          <Albums key={album._id} _id={album._id} title={album.title} date={album.date} image={album.image} />
        ))}
      </Grid>
    </Grid>
  );
};

export default ListAlbums;

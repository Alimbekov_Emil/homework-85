import { Button, Card, CardHeader, CardMedia, Grid, makeStyles } from "@material-ui/core";
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";

import React from "react";
import { Link } from "react-router-dom";
import { apiURL } from "../../config";
import { useDispatch, useSelector } from "react-redux";
import { deleteAlbum } from "../../store/actions/adminActions";

const useStyles = makeStyles({
  card: {
    height: "100%",
    backgroundColor: "#e8e8e8",
    border: "1px solid black",
    padding: "5px",
  },
  media: {
    height: 0,
    paddingTop: "70.25%",
    borderBottom: "1px solid black",
  },
  link: {
    textDecoration: "none",
    textAlign: "center",
  },
});

const Albums = ({ title, image, date, _id }) => {
  const dispatch = useDispatch();
  const classes = useStyles();

  let cardImage = imageNotAvailable;

  if (image) {
    cardImage = apiURL + "/" + image;
  }

  const user = useSelector((state) => state.users.user);

  return (
    <Grid item xs sm md={6} lg={4}>
      <Grid component={Link} to={"/tracks/" + _id} className={classes.link}>
        <Card className={classes.card}>
          <CardMedia image={cardImage} title={title} className={classes.media} />
          <CardHeader title={title} />
          <p> Год Выпуска: {date}</p>
        </Card>
      </Grid>
      <Grid item>
        {user && user.role === "admin" && (
          <Button onClick={() => dispatch(deleteAlbum(_id))} variant="outlined" style={{ margin: "10px" }}>
            Delete
          </Button>
        )}
      </Grid>
    </Grid>
  );
};

export default Albums;

import React from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import { Link } from "react-router-dom";
import { Button, CardMedia, makeStyles } from "@material-ui/core";
import imageNotAvailable from "../../assets/images/imageNotAvailable.png";
import { apiURL } from "../../config";
import { useDispatch, useSelector } from "react-redux";
import { deleteArtist } from "../../store/actions/adminActions";

const useStyles = makeStyles({
  card: {
    height: "100%",
    border: "2px solid #367ce3",
  },
  media: {
    height: 0,
    paddingTop: "90.25%",
  },
  link: {
    textDecoration: "none",
    textAlign: "center",
  },
});

const Artist = ({ title, image, _id }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  let cardImage = imageNotAvailable;

  if (image) {
    cardImage = apiURL + "/" + image;
  }

  const user = useSelector((state) => state.users.user);

  return (
    <Grid item xs sm md={6} lg={3} style={{ margin: "15px 0" }}>
      <Grid item component={Link} to={"/albums/" + _id} className={classes.link} style={{ margin: "20px 0" }}>
        <Card className={classes.card}>
          <CardHeader title={title} />
          <CardMedia image={cardImage} title={title} className={classes.media} />
        </Card>
      </Grid>
      <Grid item>
        {user && user.role === "admin" && (
          <Button onClick={() => dispatch(deleteArtist(_id))} variant="outlined" style={{ margin: "10px" }}>
            Delete
          </Button>
        )}
      </Grid>
    </Grid>
  );
};

export default Artist;

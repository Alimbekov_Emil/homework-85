import { Grid, Typography } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchArtists } from "../../store/actions/artistAction";
import Artist from "./Artist";

const ListArtist = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch]);

  const artists = useSelector((state) => state.artist.artists);

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item container justify="space-between" alignItems="center">
        <Grid item>
          <Typography variant="h4">Список Артистов</Typography>
        </Grid>
      </Grid>
      <Grid item container spacing={3}>
        {artists.map((artist) => (
          <Artist key={artist._id} _id={artist._id} title={artist.title} image={artist.image} />
        ))}
      </Grid>
    </Grid>
  );
};

export default ListArtist;

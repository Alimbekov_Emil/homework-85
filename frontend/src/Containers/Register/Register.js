import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import { Avatar, Container, Grid, Link, makeStyles, Typography } from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import ButtonWithProgress from "../../Components/ButtonWithProgress/ButtonWithProgress";
import FormElement from "../../Components/Form/FormElement";
import { registerUser } from "../../store/actions/usersActions";
import FileInput from "../../Components/Form/FileInput";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  header: {
    marginBottom: theme.spacing(2),
  },
}));

const Register = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [user, setUser] = useState({
    email: "",
    password: "",
    image: "",
    displayName: "",
  });

  const loading = useSelector((state) => state.loading);
  const error = useSelector((state) => state.users.registerError);

  const inputChangeHandler = (e) => {
    const { name, value } = e.target;

    setUser((prev) => ({ ...prev, [name]: value }));
  };

  const fileChangeHandler = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setUser((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(user).forEach((key) => {
      formData.append(key, user[key]);
    });

    dispatch(registerUser(formData));
  };

  const getFieldError = (fieldName) => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
    <Container component="section" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5" className={classes.header}>
          Sign up
        </Typography>
        <Grid container spacing={1} direction="column" component="form" onSubmit={submitFormHandler}>
          <FormElement
            label="Email"
            name="email"
            value={user.email}
            onChange={inputChangeHandler}
            autoComplete="new-email"
            type="text"
            error={getFieldError("email")}
            required
          />
          <FormElement
            label="Password"
            name="password"
            value={user.password}
            onChange={inputChangeHandler}
            autoComplete="new-password"
            type="password"
            error={getFieldError("password")}
            required
          />
          <FormElement
            label="Display Name"
            name="displayName"
            value={user.displayName}
            onChange={inputChangeHandler}
            error={getFieldError("displayName")}
            type="text"
            required
          />

          <Grid item xs>
            <FileInput name="image" label="Avatar Image" onChange={fileChangeHandler} />
          </Grid>

          <Grid item xs>
            <ButtonWithProgress
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Sign Up
            </ButtonWithProgress>
          </Grid>
          <Grid item container justify="flex-end">
            <Grid item>
              <Link component={RouterLink} variant="body2" to="/login">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Register;

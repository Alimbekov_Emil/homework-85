import { Grid, Typography } from "@material-ui/core";
import React, { useState } from "react";
import ButtonWithProgress from "../../Components/ButtonWithProgress/ButtonWithProgress";
import FormElement from "../../Components/Form/FormElement";
import FileInput from "../../Components/Form/FileInput";
import { useDispatch } from "react-redux";
import { createArtist } from "../../store/actions/artistAction";

const AddNewArtist = () => {
  const dispatch = useDispatch();
  const [artist, setArtist] = useState({
    title: "",
    info: "",
    image: "",
  });

  const inputChangeHandler = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setArtist((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const fileChangeHandler = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setArtist((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(artist).forEach((key) => {
      formData.append(key, artist[key]);
    });

    dispatch(createArtist(formData));
  };

  return (
    <form onSubmit={submitFormHandler}>
      <Grid container direction="column" spacing={2}>
        <Typography variant="h4">Create Artist</Typography>
        <Grid item xs>
          <FormElement
            required
            label="Title"
            name="title"
            value={artist.title}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs>
          <FormElement
            multiline
            rows={3}
            label="Info"
            name="info"
            value={artist.info}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs>
          <FileInput name="image" label="Image" onChange={fileChangeHandler} />
        </Grid>

        <Grid item xs>
          <ButtonWithProgress type="submit" color="primary" variant="contained">
            Create
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddNewArtist;

import { Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import ButtonWithProgress from "../../Components/ButtonWithProgress/ButtonWithProgress";
import FormElement from "../../Components/Form/FormElement";
import { useDispatch, useSelector } from "react-redux";
import { fetchAlbums } from "../../store/actions/albumsActions";
import { createTracks } from "../../store/actions/tracksActions";

const AddNewTrack = () => {
  const dispatch = useDispatch();

  const [track, setTrack] = useState({
    title: "",
    duration: "",
    number: "",
    album: "",
    link: "",
  });

  useEffect(() => {
    dispatch(fetchAlbums());
  }, [dispatch]);

  const albums = useSelector((state) => state.albums.albums);

  const inputChangeHandler = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setTrack((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();
    dispatch(createTracks({ ...track }));
  };

  return (
    <form onSubmit={submitFormHandler}>
      <Grid container direction="column" spacing={2}>
        <Typography variant="h4">Create Track</Typography>
        <Grid item xs>
          <FormElement
            required
            label="Title"
            name="title"
            value={track.title}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs>
          <FormElement
            required
            label="Duration"
            name="duration"
            value={track.duration}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs>
          <FormElement
            type="number"
            required
            label="Number"
            name="number"
            value={track.number}
            onChange={inputChangeHandler}
          />
        </Grid>

        <Grid item xs>
          <FormElement
            select
            options={albums}
            label="Albums"
            name="album"
            value={track.album}
            onChange={inputChangeHandler}
          />
        </Grid>

        <Grid item xs>
          <FormElement
            multiline
            rows={3}
            label="You Tube Link"
            name="link"
            value={track.link}
            onChange={inputChangeHandler}
          />
        </Grid>

        <Grid item xs>
          <ButtonWithProgress type="submit" color="primary" variant="contained">
            Create
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddNewTrack;

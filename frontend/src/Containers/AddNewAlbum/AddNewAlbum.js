import { Grid, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import ButtonWithProgress from "../../Components/ButtonWithProgress/ButtonWithProgress";
import FormElement from "../../Components/Form/FormElement";
import FileInput from "../../Components/Form/FileInput";
import { useDispatch, useSelector } from "react-redux";
import { fetchArtists } from "../../store/actions/artistAction";
import { createAlbum } from "../../store/actions/albumsActions";

const AddNewAlbum = () => {
  const dispatch = useDispatch();

  const [album, setAlbum] = useState({
    title: "",
    artist: "",
    image: "",
    date: "",
  });

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch]);

  const artists = useSelector((state) => state.artist.artists);

  const inputChangeHandler = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setAlbum((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const fileChangeHandler = (e) => {
    const name = e.target.name;
    const file = e.target.files[0];

    setAlbum((prevState) => ({
      ...prevState,
      [name]: file,
    }));
  };

  const submitFormHandler = (e) => {
    e.preventDefault();

    const formData = new FormData();

    Object.keys(album).forEach((key) => {
      formData.append(key, album[key]);
    });

    dispatch(createAlbum(formData));
  };

  return (
    <form onSubmit={submitFormHandler}>
      <Grid container direction="column" spacing={2}>
        <Typography variant="h4">Create Album</Typography>
        <Grid item xs>
          <FormElement
            required
            label="Title"
            name="title"
            value={album.title}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item xs>
          <FormElement
            label="Year of issue"
            name="date"
            type="number"
            value={album.date}
            onChange={inputChangeHandler}
          />
        </Grid>

        <Grid item xs>
          <FormElement
            select
            options={artists}
            label="Artist"
            name="artist"
            value={album.artist}
            onChange={inputChangeHandler}
          />
        </Grid>

        <Grid item xs>
          <FileInput name="image" label="Image" onChange={fileChangeHandler} />
        </Grid>

        <Grid item xs>
          <ButtonWithProgress type="submit" color="primary" variant="contained">
            Create
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddNewAlbum;

import { Button, Card, CardHeader, Grid, Modal } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteTracks } from "../../store/actions/adminActions";

const Track = ({ id, link, number, duration, title, onClick }) => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const user = useSelector((state) => state.users.user);

  return (
    <Grid item>
      <Card style={{ padding: "10px", border: "2px solid black" }}>
        <CardHeader title={title} />
        <Grid item>
          <p style={{ fontSize: "18px" }}> Номер Трека : {number}</p>
          <p style={{ fontSize: "18px" }}> Продолжительность : {duration}</p>
          <Button variant="outlined" color="primary" style={{ margin: "10px" }} onClick={() => onClick(id)}>
            Прослушать
          </Button>
          {user && user.role === "admin" && (
            <Button onClick={() => dispatch(deleteTracks(id))} variant="outlined" style={{ margin: "10px" }}>
              Delete
            </Button>
          )}
        </Grid>
        {link ? (
          <Button onClick={handleOpen} variant="outlined" color="primary" style={{ margin: "10px" }}>
            Get YouTube
          </Button>
        ) : null}

        <Modal
          open={open}
          onClick={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <iframe
            style={{ margin: "5% 0 5% 10%" }}
            width="80%"
            height="80%"
            src={"https://www.youtube.com/embed/" + link}
            title={"YouTube video player" + link}
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </Modal>
      </Card>
    </Grid>
  );
};

export default Track;

import { Grid, Typography } from "@material-ui/core";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchTracks } from "../../store/actions/tracksActions";
import { postTrackHistory } from "../../store/actions/trackHistoryActions";
import Track from "./Track";

const ListTracks = (props) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchTracks(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  const tracks = useSelector((state) => state.tracks.tracks);

  const trackHistoryPush = (id) => {
    dispatch(postTrackHistory(id));
  };

  return (
    <Grid container spacing={2} direction="column">
      {tracks.length > 0 ? (
        <Typography variant="h4">{tracks[0].album.title}</Typography>
      ) : (
        <Typography variant="h6"> Список Tреков Пуст</Typography>
      )}
      {tracks.map((track) => {
        return (
          <Track
            key={track._id}
            id={track._id}
            title={track.title}
            link={track.link}
            number={track.number}
            duration={track.duration}
            onClick={trackHistoryPush}
          />
        );
      })}
    </Grid>
  );
};

export default ListTracks;

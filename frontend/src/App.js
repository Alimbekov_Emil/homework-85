import { Redirect, Route, Switch } from "react-router-dom";
import AppToolbar from "./Components/UI/AppToolbar/AppToolbar";
import ListArtist from "./Containers/ListArtists/ListArtist";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Container } from "@material-ui/core";
import ListAlbums from "./Containers/ListAlbums/ListAlbums";
import ListTracks from "./Containers/ListTracks/ListTracks";
import Login from "./Containers/Login/Login";
import Register from "./Containers/Register/Register";
import TrackHistory from "./Containers/TrackHistory/TrackHistory";
import AddNewAlbum from "./Containers/AddNewAlbum/AddNewAlbum";
import AddNewArtist from "./Containers/AddNewArtist/AddNewArtist";
import AddNewTrack from "./Containers/AddNewTrack/AddNewTrack";
import { useSelector } from "react-redux";
import AdminTable from "./Containers/AdminTable/AdminTable";

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
};

const App = () => {
  const user = useSelector((state) => state.users.user);

  return (
    <>
      <CssBaseline />
      <header style={{ textAlign: "center" }}>
        <AppToolbar />
      </header>
      <main>
        <Container maxWidth="xl">
          <Switch>
            <Route path="/" exact component={ListArtist} />
            <Route path="/albums/:id" component={ListAlbums} />
            <Route path="/tracks/:id" component={ListTracks} />
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
            <Route path="/track_history" component={TrackHistory} />
            <ProtectedRoute
              path="/album/new"
              component={AddNewAlbum}
              isAllowed={user && user.role !== null}
              redirectTo="/login"
            />
            <ProtectedRoute
              path="/artist/new"
              component={AddNewArtist}
              isAllowed={user && user.role !== null}
              redirectTo="/login"
            />
            <ProtectedRoute
              path="/track/new"
              component={AddNewTrack}
              isAllowed={user && user.role !== null}
              redirectTo="/login"
            />
            <ProtectedRoute
              path="/table/admin-table"
              exact
              component={AdminTable}
              isAllowed={user && user.role === "admin"}
              redirectTo="/login"
            />
          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;

import { Avatar, Button, Menu, MenuItem } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { apiURL } from "../../../config";
import { logoutUser } from "../../../store/actions/usersActions";

const UserMenu = ({ user }) => {
  const dispatch = useDispatch();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  let cardImage;

  if (user.avatarImage) {
    if (user.avatarImage.indexOf("fixtures") === 0 || user.avatarImage.indexOf("uploads") === 0) {
      cardImage = apiURL + "/" + user.avatarImage;
    } else {
      cardImage = user.avatarImage;
    }
  }

  return (
    <>
      <Button onClick={handleClick} color="inherit">
        {user.displayName}
        <Avatar alt={user.displayName} src={cardImage} style={{ marginLeft: "10px" }} />
      </Button>
      <Menu anchorEl={anchorEl} onClose={handleClose} open={Boolean(anchorEl)}>
        <MenuItem component={Link} to="/track_history">
          Track History
        </MenuItem>
        <MenuItem component={Link} to="/artist/new">
          Create Artist
        </MenuItem>
        <MenuItem component={Link} to="/album/new">
          Create Album
        </MenuItem>
        <MenuItem component={Link} to="/track/new">
          Create Track
        </MenuItem>
        <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;

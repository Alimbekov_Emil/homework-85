import React from "react";
import FacebookLoginButton from "react-facebook-login/dist/facebook-login-render-props";
import FacebookIcon from "@material-ui/icons/Facebook";
import { Button } from "@material-ui/core";
import { useDispatch } from "react-redux";
import { facebookLogin } from "../../../store/actions/usersActions";

const FacebookLogin = () => {
  const dispatch = useDispatch();

  const faceBookResponse = (response) => {
    if (response.id) {
      console.log(response);
      dispatch(facebookLogin(response));
    }
  };

  return (
    <FacebookLoginButton
      appId="215605016675505"
      fields="name,email,picture"
      render={(props) => (
        <Button
          fullWidth
          color="primary"
          variant="outlined"
          startIcon={<FacebookIcon />}
          onClick={props.onClick}
        >
          Login with FaceBook
        </Button>
      )}
      callback={faceBookResponse}
    />
  );
};

export default FacebookLogin;

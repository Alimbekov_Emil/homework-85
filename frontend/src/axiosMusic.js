import axios from "axios";
import { apiURL } from "./config";

const axiosMusic = axios.create({
  baseURL: apiURL,
});

export default axiosMusic;

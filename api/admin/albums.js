const express = require("express");
const auth = require("../midleWare/auth");
const permit = require("../midleWare/permit");
const Album = require("../models/Album");
const router = express.Router();

router.get("/", auth, permit("admin"), async (req, res) => {
  try {
    const albums = await Album.find({ published: false });
    return res.send(albums);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/publish/:id", auth, permit("admin"), async (req, res) => {
  const album = await Album.findOne({ _id: req.params.id });

  if (!album) {
    return res.sendStatus(404);
  }

  album.published = true;

  await album.save();

  return res.send(album);
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    await Album.deleteOne({ _id: req.params.id });
    return res.send("Deleted");
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;

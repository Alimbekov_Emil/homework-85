const express = require("express");
const auth = require("../midleWare/auth");
const permit = require("../midleWare/permit");
const Track = require("../models/Track");
const router = express.Router();

router.get("/", auth, permit("admin"), async (req, res) => {
  try {
    const tracks = await Track.find({ published: false });
    return res.send(tracks);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    await Track.deleteOne({ _id: req.params.id });
    return res.send("Deleted");
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/publish/:id", auth, permit("admin"), async (req, res) => {
  const track = await Track.findOne({ _id: req.params.id });

  if (!track) {
    return res.sendStatus(404);
  }

  track.published = true;

  await track.save();

  return res.send(track);
});

module.exports = router;

const express = require("express");
const auth = require("../midleWare/auth");
const permit = require("../midleWare/permit");
const Artist = require("../models/Artist");
const router = express.Router();

router.get("/", auth, permit("admin"), async (req, res) => {
  try {
    const artist = await Artist.find({ published: false });
    return res.send(artist);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    await Artist.deleteOne({ _id: req.params.id });
    return res.send("Deleted");
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/publish/:id", auth, permit("admin"), async (req, res) => {
  const artist = await Artist.findOne({ _id: req.params.id });

  if (!artist) {
    return res.sendStatus(404);
  }

  artist.published = true;

  await artist.save();

  return res.send(artist);
});

module.exports = router;

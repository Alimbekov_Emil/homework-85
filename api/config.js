const path = require("path");
const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, "public/uploads"),
  db: {
    url: "mongodb://localhost/musicApi",
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
  facebook: {
    appId: "215605016675505",
    appSecret: "64ae8bb91f52810f90c14b4788a0abad",
  },
};

const mongoose = require("mongoose");

const TrackSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    duration: {
      type: String,
      required: true,
    },
    number: {
      type: Number,
      required: true,
      unique: true,
    },
    published: {
      type: String,
      required: true,
      default: false,
      enum: [true, false],
    },
    album: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Album",
      required: true,
    },
    link: String,
  },
  {
    versionKey: false,
  }
);

const Track = mongoose.model("Track", TrackSchema);
module.exports = Track;

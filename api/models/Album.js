const mongoose = require("mongoose");

const AlbumSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
    artist: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Artist",
      required: true,
    },
    published: {
      type: String,
      required: true,
      default: false,
      enum: [true, false],
    },
    date: Number,
    image: String,
  },
  {
    versionKey: false,
  }
);

const Album = mongoose.model("Album", AlbumSchema);
module.exports = Album;

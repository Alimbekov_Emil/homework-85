const path = require("path");
const express = require("express");
const Album = require("../models/Album");
const multer = require("multer");
const { nanoid } = require("nanoid");
const config = require("../config");
const auth = require("../midleWare/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    if (req.query.artist) {
      const albumsArtist = await Album.find({ artist: req.query.artist, published: true })
        .populate("artist", "title")
        .sort({ date: 1 });
      return res.send(albumsArtist);
    } else {
      const albums = await Album.find({ published: true });
      res.send(albums);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const album = await Album.findOne({ _id: req.params.id }).populate("artist", "title info");
    return res.send(album);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  const albumData = {
    title: req.body.title,
    artist: req.body.artist,
    date: req.body.date,
  };
  try {
    if (req.file) {
      albumData.image = "uploads/" + req.file.filename;
    }

    const album = new Album(albumData);
    await album.save();
    res.send(album);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;

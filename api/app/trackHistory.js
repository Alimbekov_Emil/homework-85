const express = require("express");
const router = express.Router();

const TrackHistory = require("../models/TrackHistory");
const auth = require("../midleWare/auth");

router.get("/track_history", auth, async (req, res) => {
  try {
    const trackHistory = await TrackHistory.find({ user: req.user._id })
      .populate({ path: "track", populate: { path: "album", populate: { path: "artist" } } })
      .sort({ datetime: -1 });
    return res.send(trackHistory);
  } catch (e) {
    return res.status(401).send({ error: "Wrong Token!" });
  }
});

router.post("/track_history", auth, async (req, res) => {
  try {
    const datetime = new Date().toISOString();

    const trackHistory = new TrackHistory({ user: req.user._id, track: req.body.track, datetime });

    await trackHistory.save();

    return res.send(trackHistory);
  } catch (e) {
    return res.sendStatus(400);
  }
});

module.exports = router;

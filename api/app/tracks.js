const express = require("express");
const Track = require("../models/Track");
const Album = require("../models/Album");

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    if (req.query.album) {
      const trackAlbum = await Track.find({ album: req.query.album, published: true })
        .populate("album", "title")
        .sort({ number: 1 });
      return res.send(trackAlbum);
    } else if (req.query.artist) {
      const albumArtist = await Album.find({ artist: req.query.artist, published: true });

      const array = albumArtist.map(async (album) => {
        const track = await Track.find({ album: album._id });

        if (track) {
          return track;
        }
      });

      res.send(await Promise.all(array));
    } else {
      const track = await Track.find({ published: true });
      return res.send(track);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", async (req, res) => {
  const tracksData = req.body;
  try {
    const track = new Track(tracksData);

    if (tracksData.link) {
      const link = tracksData.link.substr(17);
      track.link = link;
    }

    await track.save();
    res.send(track);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;

const express = require("express");
const { nanoid } = require("nanoid");
const User = require("../models/User");
const path = require("path");
const multer = require("multer");
const config = require("../config");
const axios = require("axios");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const user = await User.find();
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post("/", upload.single("image"), async (req, res) => {
  const userData = {
    email: req.body.email,
    password: req.body.password,
    displayName: req.body.displayName,
  };
  try {
    if (req.file) {
      userData.avatarImage = "uploads/" + req.file.filename;
    }

    const user = new User(userData);

    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.post("/sessions", async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });

    if (!user) {
      return res.status(401).send({ error: "Displayed an incorrect email or password" });
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(401).send({ error: "Displayed an incorrect email or password" });
    }

    user.generateToken();

    await user.save();

    return res.send({ message: "Email and password correct!", user });
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.delete("/sessions", async (req, res) => {
  const token = req.get("Authorization");
  const success = { message: "Success" };

  if (!token) return res.send(success);

  const user = await User.findOne({ token });

  if (!user) return res.send(success);

  user.generateToken();

  await user.save();

  return res.send(success);
});

router.use("/facebookLogin", async (req, res) => {
  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + "|" + config.facebook.appSecret;

  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({ message: "Facebook token incorected" });
    }

    if (response.data.data["user_id"] !== req.body.id) {
      return res.status(401).send({ global: "User ID incorected" });
    }

    let user = await User.findOne({ email: req.body.email });

    if (!user) {
      user = await User.findOne({ facebookId: req.body.id });
    }

    if (!user) {
      user = new User({
        email: req.body.email || nanoid(),
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name,
        avatarImage: req.body.picture.data.url,
      });
    }

    user.generateToken();
    await user.save();

    return res.send({ message: "Success", user });
  } catch (e) {
    return res.status(401).send({ global: "Facebook token incorected" });
  }
});

module.exports = router;

const path = require("path");
const express = require("express");
const Artist = require("../models/Artist");
const multer = require("multer");
const { nanoid } = require("nanoid");
const config = require("../config");
const auth = require("../midleWare/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const artist = await Artist.find({ published: true });
    res.send(artist);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  const artistData = {
    title: req.body.title,
    info: req.body.info,
  };

  try {
    if (req.file) {
      artistData.image = "uploads/" + req.file.filename;
    }

    const artist = new Artist(artistData);

    await artist.save();
    res.send(artist);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;

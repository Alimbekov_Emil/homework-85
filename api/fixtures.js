const mongoose = require("mongoose");
const config = require("./config");
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const { nanoid } = require("nanoid");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [scriptonite, miyagi, basta] = await Artist.create(
    {
      title: "Скриптонит",
      info: "Необходим логопед",
      image: "fixtures/scriptonite.jpeg",
      published: true,
    },
    { title: "Myagi", info: "Бородатый человек", image: "fixtures/myagi.jpeg", published: true },
    { title: "Баста", info: "Приму на газгольдер", image: "fixtures/basta.jpeg", published: true }
  );

  const [partyOnTheStreet, hajime, bastaOne] = await Album.create(
    {
      title: "Праздник на улице 36",
      artist: scriptonite,
      image: "fixtures/albumScriptonite.jpeg",
      published: true,
      date: 2010,
    },
    {
      title: "Hajime",
      artist: miyagi,
      image: "fixtures/albumMyagi.jpeg",
      published: true,
      date: 2006,
    },
    {
      title: "Баста 1",
      artist: basta,
      image: "fixtures/albumBasta.jpeg",
      published: true,
      date: 2012,
    }
  );

  await Track.create(
    { title: "Напомни", duration: "3:50", number: 1, published: true, album: partyOnTheStreet },
    { title: "Цепи", duration: "3:50", number: 2, published: true, album: partyOnTheStreet },
    { title: "Поворот", duration: "3:50", number: 3, published: true, album: partyOnTheStreet },
    { title: "Первый", duration: "3:50", number: 4, published: true, album: partyOnTheStreet },
    { title: "Темно", duration: "3:50", number: 5, published: true, album: partyOnTheStreet },

    { title: "OneLove", duration: "3:51", number: 1, published: true, album: hajime },
    { title: "Бэйба судьба", duration: "3:52", number: 2, published: true, album: hajime },
    { title: "God Bless", duration: "3:53", number: 3, published: true, album: hajime },
    { title: "Hajime (INTRO)", duration: "3:54", number: 4, published: true, album: hajime },
    { title: "Рапапам", duration: "3:55", number: 5, published: true, album: hajime },

    { title: "Гонки", duration: "3:56", number: 1, published: true, album: bastaOne },
    { title: "Мама", duration: "3:57", number: 2, published: true, album: bastaOne },
    { title: "Skit", duration: "3:58", number: 3, published: true, album: bastaOne },
    { title: "Меньше слов", duration: "3:59", number: 4, published: true, album: bastaOne },
    { title: "Осень", duration: "3:11", number: 5, published: true, album: bastaOne }
  );

  await User.create(
    {
      email: "emil",
      password: "12345",
      token: nanoid(),
      role: "user",
      displayName: "Веселый Лис",
      avatarImage: "fixtures/albumScriptonite.jpeg",
    },
    {
      email: "admin",
      password: "12345",
      token: nanoid(),
      role: "admin",
      displayName: "Администратор",
      avatarImage: "fixtures/albumBasta.jpeg",
    }
  );

  await mongoose.connection.close();
};

run().catch(console.error);
